% last updated in April 2002 by Antje Endemann
% Based on CVPR 07 and LNCS, with modifications by DAF, AZ and elle, 2008 and AA, 2010, and CC, 2011; TT, 2014; AAS, 2016

\documentclass[runningheads]{llncs}
\usepackage{graphicx}
\usepackage{amsmath,amssymb} % define this before the line numbering.
\usepackage{ruler}
\usepackage{color}
\usepackage{cite}
\usepackage[width=122mm,left=12mm,paperwidth=146mm,height=193mm,top=12mm,paperheight=217mm]{geometry}

\def\eqnvspace{{\vspace{-2mm}}}
\def\figvspace{{\vspace{-3mm}}}
\newcommand{\RNum}[1]{\uppercase\expandafter{\romannumeral #1\relax}} 
\newcommand{\Rnum}[1]{\lowercase\expandafter{\romannumeral #1\relax}} 

\begin{document}
% \renewcommand\thelinenumber{\color[rgb]{0.2,0.5,0.8}\normalfont\sffamily\scriptsize\arabic{linenumber}\color[rgb]{0,0,0}}
% \renewcommand\makeLineNumber {\hss\thelinenumber\ \hspace{6mm} \rlap{\hskip\textwidth\ \hspace{6.5mm}\thelinenumber}}
% \linenumbers
\pagestyle{headings}
\mainmatter
\def\ECCV16SubNumber{1476} % Insert your submission number here

\title{Supplementary Material for: Multi-Task Convolutional Neural Network for Face Recognition in Constrained Environment} % Replace with your title

\titlerunning{ECCV-16 submission ID \ECCV16SubNumber}

\authorrunning{ECCV-16 submission ID \ECCV16SubNumber}

\author{Anonymous ECCV submission}
\institute{Paper ID \ECCV16SubNumber}



\maketitle
\section{Convergence Rates for Different Methods}
In this paper, we have compared three different CNN methods on face recognition.
They are single-task learning for identity classification (ST:id), multi-task learning for identity classification with pose, expression, and illumination classification as the side tasks (MT:id+all), and pose-directed multi-task learning which adds a sub-network to group different poses to learn pose-specific identity features in addition to generic identity features (MT:pose-directed). 
Here we recall the objective function of our pose-directed MT-CNN. 
\begin{equation}
\begin{split}
\underset{{\bf{W}}^{d0,dg,p,e,m}} {\mathrm{argmin}} \sum_{i=1}^{N} l(y_i^{d}, f({\bf{x}}_i; {\bf{W}}^{d0})) + \frac{1}{G}\sum_{g=1}^{G}\sum_{i=1}^{N_g} l(y_i^{d}, f({\bf{x}}_i; {\bf{W}}^{dg})) + \\
\alpha \sum_{i=1}^{N} l(y_i^{p}, f({\bf{x}}_i; {\bf{W}}^{p})) + 
\beta \sum_{i=1}^{N} l(y_i^{e}, f({\bf{x}}_i; {\bf{W}}^{e})) +
\gamma \sum_{i=1}^{N} l(y_i^{m}, f({\bf{x}}_i; {\bf{W}}^{m})),
\end{split}
\label{eqn:obj_3}
\end{equation}
where $\alpha=\beta=\gamma=0.5$ and $G=5$.
The loss consists of nine parts. 
We use $l_d$ to represent the loss for the main task of identity classification, $l_p$, $l_e$, and $l_m$ are the losses for the side tasks of pose, expression, and illumination classification respectively, and $l_{1,..,5}$ denotes the loss of identity classification for five different pose groups. 
The above three methods differs in the number of objectives. 
Specifically, only $l_d$ is used for ST identity classification, and $l_d$, $l_p$, $l_e$, $l_m$ are used for MT learning with three side tasks. 
The regularization term is omitted. 

As identity classification is considered as the main task, which is shared among three different objectives. 
We compare the loss of the main task. 
As shown in Figure~\ref{fig:maintask}, the single-task learning converges the fastest among three methods, which is not surprising because all weights updating are geared to minimize the only objective. 
However, when we add three side tasks, the network will learn the weights jointly to optimize four different objectives, resulting in a smaller convergence rate. 
For the proposed pose-directed MT-CNN model, the network is responsible to balance the objectives of nine objectives. 
Therefore, the convergence rate is further reduced. 
Finally, the losses of all three methods are becoming similar as the number of epochs increases. 
%This trend represents the difficulty of the network structures. 
By leveraging the additional annotations for the side tasks, the network is able to learn more discriminative identity features for face recognition. 

\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.6\textwidth]{Figures/maintask.pdf}
\end{center}
\vspace{-5mm}
\caption{Comparison of $l_d$ on three different methods.}
\vspace{-5mm}
\label{fig:maintask}
\end{figure}


It is also interesting to compare the convergence rates of different side tasks. 
Figure~\ref{fig:sidetasks} compares $l_d$, $l_p$, $l_e$, and $l_m$ on MT-CNN and pose-directed MT-CNN. 
First, similar to the loss of main task in Figure~\ref{fig:maintask}, the convergence rates of all side tasks reduced from MT-CNN to pose-directed MT-CNN, and the relative trend among three side tasks are the same for both methods. 
Second, pose classification is the easiest task so it converges fastest among all tasks. 
Third, expression classification actually converges faster than illumination classification. 
This is unexpected as the accuracy of illumination classification is much higher than that of expression classification. 
One potential reason for this phenomenon might be that due to unbalanced training samples for different expressions, there might be overfitting in learning the expression classifier, which results in relatively small training loss and large testing error.


%expression classification, due to its lack of balanced training samples for difficult expressions, the training process is slightly overfitting so that the loss is relatively small. 

\begin{figure}
\begin{center}
\includegraphics[width=0.6\textwidth]{Figures/sidetasks.pdf}
\end{center}
\vspace{-5mm}
\caption{Comparison of $l_p$, $l_e$, and $l_m$ for MT-CNN and pose-directed MT-CNN.}
\vspace{-5mm}
\label{fig:sidetasks}
\end{figure}

%For the proposed pose-directed MT-CNN model, we analysis the loss of identity classification for different pose groups where group $1$ to $5$ represents left profile, left half-profile, frontal, right half-profile, and right profile, respectively. 
%Figure~\ref{fig:pose} shows the identity classification losses for each pose group. 
%And Table~\ref{tab:pose} shows the face identification rate for each pose groups. 
%Surprisingly, we found that face recognition performance on right profiles (including right profile and half-profile) is better than left profiles (including left profile and half-profile).
%And this is true for all three methods. 
%The above conclusion is also supported by the evidence that the losses of right profiles are smaller than those of left profiles as shown in Figure~\ref{fig:pose}. 
% 
%
%\begin{figure}
%\begin{center}
%\includegraphics[width=0.6\textwidth]{Figures/pose.pdf}
%\end{center}
%\vspace{-5mm}
%\caption{Comparison of $l_{1,..,5}$ for pose-directed MT-CNN.}
%\label{fig:pose}
%\vspace{-5mm}
%\end{figure}
%
%\begin{table}
%\begin{center}
%\begin{tabular}{ l|c|c|c|c|c}
%\hline 
%& left profile & left half-profile & frontal & right half-profile & right profile \\ \hline 
%ST:id & 38.1 & 42.4 & 65.1 & 44.8 & 40.1 \\ \hline
%MT:id+all & 41.7 & 46.3 & 65.4 & 49.8 & 44.2 \\ \hline
%MT:pose-directed & 46.6 & 50.1 & 71.3 & 51.9 & 49.2 \\ \hline
%\end{tabular}
%\end{center}
%\eqnvspace
%\caption{Comparison of face identification rates ($\%$) w.r.t.~different pose groups.} 
%\label{tab:pose}
%\vspace{-5mm}
%\end{table}

\section{Weights Visualization}
We investigate the reason why adding the side tasks improves face recognition performance. 
To do this, we analysis the weights for the last fully connected layer in MT-CNN with all side tasks. 
As shown in Figure $2$ (c) in the original paper, the second fully connected layer (fc$2$) is shared among all tasks, where each task learns an individual weight matrix for its classification. 
Specifically, the weight matrix sizes for each task are ${\bf{W}}^d: 1024\times200$, ${\bf{W}}^p: 1024\times13$, ${\bf{W}}^e: 1024\times6$, and ${\bf{W}}^m: 1024\times19$. 
We verify if different tasks will weight differently for fc$2$. 

We concatenate every weight matrix into ${\bf{W}} = [{\bf{W}}^d, {\bf{W}}^p, {\bf{W}}^e, {\bf{W}}^m]$ with size of $1024\times238$. 
As shown in Figure~\ref{fig:weights} (a), the weights are relatively small and it is hard to compare between different tasks. 
Therefore, we consider the relative importance of each element in fc$2$ on the final estimation of different tasks. 
Specifically, we compute the mean of the absolute value of each row in ${\bf{W}}^d$ and generate a $1024$-dim vector ${\bf{s}}^d$, where a larger value indicates higher importance of the corresponding fc$2$ element to identity classification. 
We then sort all values in ${\bf{s}}^d$ in an ascending order and replace the original value with its rank - a number in $[1,1024]$ where larger ones indicate higher importance.
A new matrix $\hat{{\bf{W}}^d}$, which is of the same size as ${\bf{W}}_d$, is generated where each row is the same value as the corresponding element in ${\bf{s}}^d$.
Similarly, for the other three weight matrices, we compute $\hat{{\bf{W}}^p}$, $\hat{{\bf{W}}^e}$, and $\hat{{\bf{W}}^m}$ and concatenate them into a matrix $\hat{{\bf{W}}} = [\hat{{\bf{W}}^d}, \hat{{\bf{W}}^p}, \hat{{\bf{W}}^e}, \hat{{\bf{W}}^m}]$, as shown in Figure~\ref{fig:weights} (b). 
Finally, we sort the rows of $\hat{\bf{W}}$ according to the values in $\hat{{\bf{W}}^d}$, which results in Figure~\ref{fig:weights} (c). 


%Therefore, for each task, we compute the mean value of the absolute weights of each row and assign the numbers from $1$ to $1024$ to represent the importance of each element in fc$2$. 
%The re-weighted $\bf{W}$ is shown in Figure~\ref{fig:weights} (b). 
%Finally, we sort the rows of re-weighted $\bf{W}$ according to its importance to the main task, which results in Figure~\ref{fig:weights} (c). 
%The last $100$ rows show the portion when the summation of the importance factors of all tasks are maximized. 

As shown in Figure~\ref{fig:weights} (c), fc$2$ is learnt to be separable for the main task and the side tasks. 
For those elements that are weighted large for the identity classification, the corresponding weights for the classification of side tasks are relatively small. 
So by adding the side tasks, the network is trained to learn features (i.e., the fc$2$ layer) that are separable for each task.
Compared to single-task learning where all elements in fc$2$ are contributing to one task only, the separability in multi-task learning acts as if adding a sparsity constraint on fc$2$ for the main task of identity classification, which results in a more compact and discriminative features that has a better generalization capability. 
Therefore, the performance of face recognition increases with the help of side tasks in MT-CNN. 



%As shown in Figure~\ref{fig:weights} (c), fc$2$ is learnt to be separable for different tasks, as each task has a dominant portion where the weights are larger than that of other tasks. 
%Among all side tasks, illumination classification shares the least number of common elements in fc$2$ with other tasks, which is understandable as illumination changes the intensities of the image but there is no deformation of the face occurs. 
%For expression and pose classification, more common elements are shared with the main task, which means that the variations of expression and pose are more correlated with the learning of identity features. 
%Moreover, as shown in the last $100$ rows from Figure~\ref{fig:weights} (c), no elements in fc$2$ are dominant for all tasks. 
%Therefore, the shared features are learnt to be separable for different tasks in multi-task learning. 


\begin{figure}
\centering
\begin{tabular}{@{}c@{}c@{}c@{}}
\includegraphics[width=0.33\textwidth]{Figures/original_w.pdf} &
\includegraphics[width=0.33\textwidth]{Figures/weighted_w.pdf} &
\includegraphics[width=0.33\textwidth]{Figures/w_rank.pdf} \\
(a) & (b) & (c)\\
\end{tabular}
\figvspace
\caption{Weights visualization. Horizontally from left to right, the four regions correspond to the tasks of identity, pose, expression, and illumination classification, respectively. Yellow rows (i.e., elements of the fc$2$ layer) have higher contributions/weights to the main task or one of the three side tasks.}
\label{fig:weights} 
\figvspace
\end{figure}



\section{Face Identification Examples}
Figure~\ref{fig:examples} shows several examples of face identification. 
When we consider face recognition under large variations of pose, expression, illumination, and session, it is very challenging. 
However, the proposed method can still handle this large variation to some extend. 
For example, in Figure~\ref{fig:examples} (a), the test image is recorded with large pose, scream expression, different lighting condition, and in different session (without glasses). 
The same identity is still recognized. 
In Figure~\ref{fig:examples} (d), when the algorithm fails to recognize the test image, we can see that the rank-$1$ subject shows similar expression (mouse region) as the test image which causes the problem. 

\begin{figure}
\begin{center}
\includegraphics[width=0.9\textwidth]{Figures/examples.pdf}
\end{center}
\figvspace
\caption{Examples of several test images with the top five identified face images from the gallery set. The correct identity is shown in the green box.}
\label{fig:examples}
\figvspace
\end{figure}




%\bibliographystyle{splncs}
%\bibliography{egbib}
\end{document}


