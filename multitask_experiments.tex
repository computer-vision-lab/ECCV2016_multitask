\section{Experiments}
In this section, we present our experimental settings and results. 
The main purpose of our experiments is to demonstrate, under the state-of-the-art CNN framework, how various MTL techniques (e.g., MT-CNN and task-directed MT-CNN) can contribute to face recognition on the full set of the Multi-PIE dataset. % and evaluate the performance of the proposed task-directed MT-CNN. 
The most relevant work is the deep multi-task framework for facial landmark detection~\cite{zhang2014facial}, which is similar to the MT-CNN in Figure~\ref{fig:overview_1} (c).
Note that there are other recent work that aim to improve face recognition performance, with novel extensions of CNN~\cite{xiong2015conditional,zhu2014multi}.
We view them as orthogonal development of CNN and hence not directly comparable to our direction of MTL for CNN. 
%In this paper, we apply similar structure in~\cite{zhang2014facial} to face recognition and proved the effectiveness of the proposed approach over~\cite{zhang2014facial}. 
%In this section, we will first evaluate how different network structures in Figure~\ref{fig:overview_1} influences the performance of each task, especially face recognition, and also compare MTL with single-task learning. 
%Second, we evaluate the contribution of task-directed MTL-CNN. 

\subsection{Experimental Settings}
{\bf{Multi-PIE}} The Multi-PIE dataset consists of $754,200$ images of $337$ subjects recorded in four sessions. 
Each subject was recorded under $15$ different cameras with $13$ at head height spaced at $15^\circ$ intervals and $2$ above the head to simulate a surveillance camera view. 
For each of these cameras, subjects were imaged under $19$ different illuminations. 
In each session, subjects were captured with $2$ or $3$ expressions, resulting in $6$ different expressions across all sessions. 
In our work, we select all data for our experiments. 
For the two poses where the cameras are above the head, their poses are labeled as $-30^\circ$ and $30^\circ$ respectively. 
The images for the first $200$ subjects are used for training. 
The remaining $137$ subjects are used for testing.
One image with frontal pose, neutral illumination, and neutral expression for each subject in the test set is selected as the gallery set and the remaining are used as the probe set.
The same experimental setup is used in {\it all} our experiments. 
As shown in Table~\ref{tab_multipie}, no prior work have used the entire Multi-PIE dataset for face recognition. 

\noindent{\bf{Implementation details}}
For each image, we use the landmark annotations provided in~\cite{el2013scalable} to align the face and transform to a $100\times100$ gray-scale image. 
We use Caffe~\cite{jia2014caffe} in our experiments where we implement the task-directed layer. 
The same parameters are used in all our experiments: the initial learning rate is $0.01$ and gradually decreases, $0.9$ for momentum and $0.0005$ for the weight decay. 
The weight coefficients are set to be $\alpha = \beta = \gamma = 0.5$. 
Our network is trained using mini-batch stochastic gradient descent with back propagation where the batch size is $200$. 

\noindent{\bf{Evaluation Metrics}}
Our main task is face recognition, and pose, expression, and illumination estimation are considered as side tasks. 
For face recognition, %we use the output of the last fully connected layer as the identity feature. 
the cosine distance between two identity features is used to compute the similarity of two faces. 
Each probe image is compared to all images in the gallery and the one with the maximum distance is identified. 
The rank-$1$ accuracy is reported as the face identification performance. 
For the side tasks, we estimate it directly from the output of the last fully connected layer by selecting the one with the maximum probability. 
The mean and standard deviation of the accuracies over all classes are reported. 


\subsection{Results of MT-CNN}
\noindent {\bf{Compare different network structures}} First, we study how different network structures influence the performance of face recognition.
As shown in Figure~\ref{fig:overview_1} , we design three different network structures to conduct MTL for identity, pose, expression, and illumination classification. 
Table~\ref{tab:different_nets} shows the rank-$1$ face identification rate and classification performance of all side tasks. 
Structure (c), which encourages all tasks to share as many layers as possible, achieves the best performance among the three different structures. 
The network is able to discover more correlation among different tasks when more layers are being shared. 
This helps to learn more robust identity features for face identification without reducing the classification accuracy of other tasks. 
Note that (c) also has the most number of weights to handle all these tasks. 
We have also done another set of experiments to adjust the numbers of output in each fully connected layers to ensure three network structures have the similar number of weights, in which (c) is still the best. 
Therefore, we use network (c) in the remainder of our experiments. 

\begin{table}[t!]
\begin{center}
\begin{tabular}{ lcccc}
\hline 
& rank-1 ($\%$) & pose & expression & illumination \\ \hline \hline
net (a) & 36.1 & 98.1 (1.4) & 79.0 (20.6) & 92.2 (3.5) \\ \hline
net (b) & 43.2 & 98.2 (1.4) & {\bf{80.8}} (18.6) & {\bf{92.7}} ({\bf{3.0}}) \\ \hline
net (c) & {\bf{49.8}} & {\bf{98.6}} ({\bf{1.0}}) & 80.6 ({\bf{18.2}}) & {\bf{92.7}} (3.1) \\ \hline
\end{tabular}
\end{center}
\eqnvspace
\caption{The performance of different MT-CNN structures (a), (b), (c) as shown in Figure~\ref{fig:overview_1}. We report the rank-$1$ accuracy for face recognition. For other side tasks, we report the average accuracy and standard deviation over all classes, shown in parentheses. The best performances are in bold.} 
\label{tab:different_nets}
\vspace{-5mm}
\end{table}

\noindent {\bf{Compare to single-task learning}} To evaluate the influence of MTL, we train four single tasks for identity (id), pose (pos), expression (exp), and illumination (illum) classification, respectively.
The network structure is similar to (c) where the only difference is that instead of generating four outputs at the same time, we generate only one output for each single-task CNN. 
Furthermore, in order to evaluate the influence of each side task, we add one side task at a time to the main task. 
We use ``id+pos'', ``id+exp'', and ``id+illum'' to represent these variants and compare them to the performance of adding all side tasks denoted as ``id+all'', which is essentially the network (c) in Table~\ref{tab:different_nets}.

\begin{table}[t!]
\begin{center}
\begin{tabular}{ lcccc}
\hline 
& rank-1 ($\%$) & pose & expression & illumination \\ \hline \hline
ST & 46.5 & 98.1 (1.4) & {\bf{81.8}} ({\bf{17.1}}) & {\bf{94.2}} ({\bf{2.5}}) \\ \hline
MT: id+pos & 48.6 & 98.1 (1.3) & - & - \\ \hline
MT: id+exp & 46.8 & - & 78.7 (19.0) & - \\ \hline
MT: id+illum & 48.7 & - & - & 92.2 (3.3) \\ \hline
MT: id+all & {\bf{49.8}} & {\bf{98.6}} ({\bf{1.0}}) & 80.6 (18.2) & 92.7 (3.1) \\ \hline
\end{tabular}
\end{center}
\eqnvspace
\caption{Comparison of single-task learning with multi-task learning.} 
\vspace{-5mm}
\label{tab:multitask}
\end{table}


The first row in Table~\ref{tab:multitask} shows the performance of single-task learning. 
The rank-$1$ identification rate is only $46.5\%$ without adding any side tasks. 
Among three other tasks, pose estimation is the easiest ($98.1\%$), followed by illumination estimation ($94.2\%$), and expression estimation is the most difficult ($81.8\%$). 
This is caused by two potential reasons. 
First, discriminating expression is indeed challenging due to the non-rigid face deformation in different expressions, especially when it mixes with pose variations.
Second, the distribution over different expressions is unbalanced. 
The full dataset includes five sets of neutral expression, two for smile, and one for the other four expressions, which results in insufficient training data for more challenging expressions. 

Comparing the face identification rates on single-task and multi-task learning, it is obvious that adding side tasks is always helpful for the main task, despite the fact that the performance of the side tasks might be similar or slightly worse.
By leveraging the additional annotations in side tasks, MTL helps the CNN model to learn the specific variations that might be hidden in single-task identity learning. 
The contribution of using pose or illumination as a side task is larger than that of using expression as a side task due to pose and illumination are by themselves easier to be classified compared to expression. 
This also provides guidance on how to select side tasks for MTL. 
%, which demonstrates the challenge of expression variations for face recognition.
%This is potentially due to the large intra-class variation for different expressions. 
Moreover, adding all side tasks together outperforms adding only one of them. 
The rank-$1$ accuracy of MTL with all side tasks is $3.3\%$ higher than the single-task model. 
%Therefore, the network structure proposed in~\cite{zhang2014facial} for face alignment, is also useful for multi-task face recognition. 

\subsection{Results of Task-Directed MT-CNN}
\noindent {\bf{Pose-directed MT-CNN}} 
Motivated by the fact that pose estimation is relatively accurate, we propose a pose-directed MT-CNN as shown in Figure~\ref{fig:overview_2} where the pose-directed sub-network is appended to the first fully connected layer (fc1).
%In the testing stage, the estimated pose can be used to direct the feature extraction process. 
This pose-directed MT-CNN model aims to learn two sets of identity features. 
First, the original MT-CNN aims to learn identity features (denoted as id$0$) that are robust to all pose variations. 
Second, the pose-directed part aims to jointly learn five groups of pose-specific identity features (denoted as idX meaning id$1$ to id$5$). 
During the testing stage, an input image only extracts pose-specific features from the corresponding pose group based on its pose estimation result. 

As shown in the first row of Table~\ref{tab:pose_append}, the performance of id$0$ is improved over MT-CNN without the pose-directed sub-network ($53.3\%$ vs $49.8\%$). 
This result demonstrates that our proposed pose-directed MT-CNN can even improve the discriminative ability of the generic identity features. 
In addition, the pose-specific identity features achieve similar performance to the generic features ($53.2\%$ vs $53.3\%$). 
When we use both generic and pose-specific features, due to their complementary nature, the performance of face identification is further improved to $54.2\%$, which is $4.4\%$ higher than MT-CNN with all side tasks and $7.7\%$ higher than single-task learning. 

\begin{table}[t!]
\begin{center}
\begin{tabular}{ rcccccc}
\hline 
& \multicolumn{3}{c}{rank-1 ($\%$)} & \multicolumn{3}{c}{side task classification} \\ \hline 
& id0 & idX & id0+idX & pose & expression & illumination \\ \hline \hline
pose-directed: fc1 & {\bf{53.3}} & {\bf{53.2}} & {\bf{54.2}} & {\bf{98.6}} (1.2) & {\bf{80.4}} ({\bf{17.8}}) & 92.3 (3.3) \\ \hline
pose-directed: fc2 & 51.7 & 52.1 & 52.2 & 98.5 ({\bf{1.0}}) & 80.1 (19.2) & 92.4 (3.2) \\ \hline
exp-directed: fc1 & 51.1 & 44.3 & 49.1 & 98.4 (1.5) & 79.7 (19.4) & {\bf{92.6}} ({\bf{3.0}}) \\ \hline
\end{tabular}
\end{center}
\eqnvspace
\caption{The performance of task-directed MT-CNN where the pose-directed sub-network is appended to the first and second fully connected layers respectively.} 
\label{tab:pose_append}
\vspace{-5mm}
\end{table}

\noindent {\bf{Expression-directed MT-CNN}}
We study another variant of task-directed MT-CNN by using expression estimation to direct the network for expression-specific feature extraction. 
Considering that the expression estimation is relatively low with a mean accuracy of $81.8\%$ in single task learning, we split six different expressions into three groups based on the confusion matrix and the classification of the three groups is $89.0\%$. 
Specifically, the three groups are neutral-smile, surprise-scream, and squint-disgust. 
We use the same network structure as in the pose-directed MT-CNN except $G=3$. 

As shown in the third row of Table~\ref{tab:pose_append}, the performance of exp-directed MT-CNN is worse than that of pose-directed. 
This is due to the fact that expression estimation is not as accurate as pose estimation, which can be seen from the face recognition performance of the specific features idX. 
However, the exp-directed MT-CNN improves the performance of the generic features (id$0$) where the face recognition rate is $51.1\%$ higher than MT-CNN with all side tasks ($49.8\%$). 
This observation is very important. 
As in more general case when the side task classification is not accurate, the added sub-network can still help the main task by updating the weights in the shared layers. 

\noindent {\bf{Compare different network structures}} 
We also study the question of where to append the pose-directed sub-network. 
Specifically, we evaluate the performance of appending it to the first (fc$1$) or second (fc$2$) fully connected layer. 
Figure~\ref{fig:overview_2} shows the structure of appending to fc$1$ where each group consists of two fully connected layers. 
The second structure is to append to fc$2$. 
In this case, the sub-network of each pose group has only one fully connected layer, which ensures that each task has the same number of layers as the first structure. 

As shown in the second row of Table~\ref{tab:pose_append}, the performance of appending the pose-directed part to fc$1$ outperforms that of appending to fc$2$. 
While this conclusion seems to be conflict with the result in Table~\ref{tab:different_nets} where the performance increases as more layers are shared. 
This is due to the fact that there are more correlation between the four tasks in Table~\ref{tab:different_nets}. 
However, we propose to learn two kinds of identity features that are different from each other. 
Therefore, sharing more layers will be difficult to learn the weights that can adapt to both tasks. 
In the remainder of this section, we analyze the results of appending the pose-directed part to fc$1$. 

\noindent {\bf{Face recognition under each variation}} It is also important to know the performance of face recognition w.r.t. each individual variation. 
Table~\ref{tab:recog_pose}, ~\ref{tab:recog_exp}, and~\ref{tab:recog_illum} show the face identification rate w.r.t.~pose, expression, and illumination variations, respectively. 
Note that when we consider the performance over one variation, the full ranges of the other two variations are included in the test set as well. 
For example, the performance of $\pm90^\circ$ pose includes test faces with all different expressions and illuminations. 
We compare the proposed task-directed MT-CNN with single-task CNN and MT-CNN with all side tasks. 


\begin{table}[t!]
\begin{center}
\begin{tabular}{ lcccccccc}
\hline 
& $\pm90^\circ$ & $\pm75^\circ$ & $\pm60^\circ$ & $\pm45^\circ$ & $\pm30^\circ$ & $\pm15^\circ$ & $0^\circ$ & avg\\ \hline \hline
ST: id & 34.0 & 40.0 & 43.3 & 40.5 & 46.7 & 63.0 & 69.3 & 46.5 \\ \hline
MT: id + all & 38.1 & 44.0 & 46.7 & 45.5 & 50.6 & 64.0 & 68.3 & 49.7 \\ \hline
MT: pose-directed & {\bf{43.1}} & {\bf{49.2}} & {\bf{51.4}} & {\bf{48.0}} & {\bf{54.0}} & {\bf{70.0}} & {\bf{73.9}} & {\bf{54.2}}\\ \hline 
\end{tabular}
\end{center}
\eqnvspace
\caption{Comparison of face identification rates ($\%$) w.r.t. different pose variations. Note that the average is computed over all poses, which might be different compared to the identification rate over all images due to unbalanced images from each pose (two cameras above the head are used as $\pm30^\circ$), similarly for Table~\ref{tab:recog_exp} and~\ref{tab:recog_illum}.} 
\label{tab:recog_pose}
\vspace{-5mm}
\end{table}

As shown in Table~\ref{tab:recog_pose}, the performance of face recognition decreases as the yaw angle increases. 
The MTL with all side tasks performs better than single-task learning in all poses except slightly worse in $0^\circ$. 
Our task-direct MT-CNN performs the best by a large margin compared to other two methods for all poses. 
And the improvement in larger poses is greater than in smaller poses. 
For example, the performance improvement on $\pm90^\circ$ faces is $9.1\%$ over the single-task learning and $4.6\%$ improvement on $0^\circ$ faces. 
This shows that adding these pose-specific features is capable of better recognizing faces at all poses, not just large poses.

As shown in Table~\ref{tab:recog_exp}, except for MTL with all side tasks where the performance on neutral expression is better than smile, the rank of difficulty of each expression is the same for all methods, namely smile, neutral, squint, surprise, disgust, scream from easy to hard. 
Among all six expressions, scream is the most difficult one and it has a large performance gap with other expressions. 
Again, the proposed approach performs the best among all six expressions.
However, the performance on the scream expression is still very low. 
Expression is the least studied variation on Multi-PIE. 
Most prior work only conduct experiments using neutral expression. 
From the results in Table~\ref{tab:recog_exp}, apparently more research effort is needed for expression-robust face recognition. 

\begin{table}[t!]
\begin{center}
\begin{tabular}{ lccccccc}
\hline 
& neutral & smile & surprise & squint & disgust & scream & avg\\ \hline \hline
ST: id & 49.0 & 50.2 & 48.7 & 52.2 & 43.6 & 27.5 & 45.2 \\ \hline
MT: id + all & 53.7 & 52.6 & 50.7 & 51.9 & 47.3 & 29.1 & 47.5 \\ \hline
MT: pose-directed & {\bf{57.6}} & {\bf{59.0}} & {\bf{54.8}} & {\bf{57.8}} & {\bf{52.2}} & {\bf{31.4}} & {\bf{52.1}}\\ \hline 
\end{tabular}
\end{center}
\eqnvspace
\caption{Comparison of face identification rates ($\%$) w.r.t.~different expressions.} 
\label{tab:recog_exp}
\vspace{-5mm}
\end{table}


Table~\ref{tab:recog_illum} shows the performance of face recognition under different illuminations, which is a less challenging problem compares to that under different poses or expressions. 
The performance of face images without flash ($00$) is the best best and no specific trend would be seen for images with flashes ($01-18$). 
However, the variance of performance is relatively small. 
The MTL is always better than single-task learning. 
And our method outperforms the other two methods for all illuminations. 

\begin{table}[t!]
\begin{center}
\begin{tabular}{ lcccccccccc}
\hline 
& 00 & 01 & 02 & 03 & 04 & 05 & 06 & 07 & 08 & 09 \\ \hline \hline
ST: id & 48.4 & 47.0 & 46.8 & 47.1& 46.3 & 43.8 & 45.0 & 43.4 & 44.2 & 45.8 \\ \hline
MT: id + all & 52.0 & 50.4 & 49.6 & 49.8 & 49.3 & 47.0 & 47.7 & 46.9 & 47.5 & 49.6 \\ \hline
MT: pose-directed & {\bf{56.2}} & {\bf{54.0}} & {\bf{54.2}} & {\bf{54.2}} & {\bf{53.6}}& {\bf{52.3}} & {\bf{52.3}} & {\bf{51.1}} & {\bf{51.9}} & {\bf{53.9}} \\ \hline \hline
& 10 & 11 & 12 & 13 & 14 & 15 & 16 & 17 & 18 & avg\\ \hline \hline
ST: id & 47.0 & 46.6 & 46.8 & 46.3 & 48.1 & 46.9 & 46.4 & 47.4 & 47.8 & 46.4 \\ \hline 
MT: id + all & 49.6 & 49.4 & 49.3 & 49.4 & 51.5 & 50.9 & 51.2 & 51.1 & 51.1 & 49.6 \\ \hline
MT: pose-directed & {\bf{54.7}} & {\bf{53.7}} & {\bf{54.4}} & {\bf{53.9}} & {\bf{55.5}} & {\bf{54.9}} & {\bf{55.4}} & {\bf{55.4}} & {\bf{56.0}} & {\bf{54.1}} \\ \hline
\end{tabular}
\end{center}
\eqnvspace
\caption{Comparison of face identification rates ($\%$) w.r.t.~different illuminations. Note that since illumination $00$ and $19$ are both no flash, their results are combined in $00$.} 
\figvspace
\label{tab:recog_illum}
\end{table}


\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.5\textwidth]{Figures/crank.eps}
\end{center}
\figvspace
\caption{Comparison on face identification rates ($\%$).}
\label{fig:rank}
\vspace{-5mm}
\end{figure}


Instead of only showing the rank-$1$ identification rate, we also compare the performance w.r.t.~different ranks.
Figure~\ref{fig:rank} shows the performance of three methods in the rank from $1$ to $10$. 
The MT-CNN with all side tasks outperforms the single-task CNN. 
And the proposed approach is the best among all methods. 
All experiments have validated that adding side tasks helps to improve face recognition. 
And our proposed task-directed MT-CNN model is very effective.
% and has the potential to be applied to vision problems other than face recognition. 

















