\section{The Proposed Approach}
We present the proposed approach in this section. 
First, we apply a MT-CNN architecture, which is originally designed for facial landmark detection~\cite{zhang2014facial}, for face recognition where pose, expression, and illumination estimation are considered as three side tasks.
Different from~\cite{zhang2014facial}, we carefully study the impact of sharing different numbers of layers on the face recognition performance. 
Second, we propose a novel task-directed MT-CNN architecture to tackle pose variations by separating all poses into five groups and jointly learning identity features for each group. 
During the testing stage, the estimated pose of an input image is used to automatically direct the path in the network by selecting weights from the estimated pose group. 

\subsection{Multi-Task Learning for CNN}
Given a set of $N$ training images and their labels, ${\bf{D}} = \{{\bf{x}}_i, {\bf{y}}_i\}_{i=1}^{N}$, where each label ${\bf{y}}_i = (y_i^d, y_i^p, y_i^e, y_i^m)$ is a four-tuple consisting of identity, pose, expression, and illumination labels respectively, we learn a MT-CNN model such that given a new input image $\bf{x}$, our model is able to extract identity features and estimate pose, expression, and illumination at the same time.
The identity features extracted from a face image can be used for face identification or verification. 
%This design is motivated by the fact that human beings unconsciously perform the above tasks simultaneously when presented with a face image. 

\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.7\textwidth]{Figures/overview_1.pdf}
\end{center}
\vspace{-5mm}
\caption{The MT-CNN architecture. The top part is the shared common layers. For the bottom part, we study three different network structures where the number of shared fully connected layers (shown in blue) is: zero in $(a)$, one in $(b)$, and two in $(c)$. Each color is associated with one task. Each of the final output is connected to a softmax loss layer, omitted for simplification, for final estimation.} 
% This layer is omitted for simplification.}
\label{fig:overview_1}
\vspace{-2mm}
\end{figure}

%Inspired by the multi-task framework for facial landmark detection~\cite{zhang2014facial}, 
The network structure of our MT-CNN is shown in Figure~\ref{fig:overview_1}. 
The input is a $100\times100$ gray-scale face image. 
The network consists of three convolutional layers, three max-pooling layers, and three fully connected layers, where the parameter settings can be found in Figure~\ref{fig:overview_1}.
It has been demonstrated that the weights in early layers acts like edge detectors, making them apt for sharing between different tasks. 
%TODO weights or parameters? (weights)
For later layers, the network learns more specific features for high-level tasks. 
One interesting question is that, in the context of MT-CNN, how many fully connected layers should be shared among different tasks. 
For example, only one fully connect layer is shared in the multi-task network proposed in~\cite{zhang2014improving} for face detection. 
The work in~\cite{zhang2014facial} proposes to share all fully connected layers except the last one for facial landmark detection. 
% TODO: are these two work using similar structure as Fig.1? If not, contrast it, if yes, say it. (added to next paragraph)

We answer this question by designing three different network structures as shown in Figure~\ref{fig:overview_1} (a), (b), (c), where all tasks sharing zero, one, and two fully connected layers, respectively.
Figure~\ref{fig:overview_1} (b) is similar to~\cite{zhang2014improving} and (c) is similar to~\cite{zhang2014facial}, except that the layer parameters are different. 
Taking the network (a) as an example, the first fully connected layer for each task is generated from the last convolutional layer. 
Therefore, no fully connected layer is shared among these tasks. 
Similarly, one fully connected layer is shared in (b) and two in (c). 
The output dimension is designed such that the summation of all tasks at each layer is the same among different network structures. 
A softmax loss layer is appended to the final output for each classification task. 
%The number of output is equal to the number of classes for each task. 
For all layers in our network, rectified linear unit~\cite{nair2010rectified} is used as the activation function. 
And dropout~\cite{hinton2012improving} is applied in the first two fully connected layers. 

Given the training set ${\bf{D}}$, our MT-CNN learning can be formulated as minimizing the combined loss of all tasks w.r.t.~the network parameters, i.e., 
\begin{equation}
\begin{split}
\underset{{\bf{W}}^{d,p,e,m}} {\mathrm{argmin}} \sum_{i=1}^{N} l(y_i^{d}, f({\bf{x}}_i; {\bf{W}}^{d})) + 
\alpha \sum_{i=1}^{N} l(y_i^{p}, f({\bf{x}}_i; {\bf{W}}^{p})) + \\
\beta \sum_{i=1}^{N} l(y_i^{e}, f({\bf{x}}_i; {\bf{W}}^{e})) +
\gamma \sum_{i=1}^{N} l(y_i^{m}, f({\bf{x}}_i; {\bf{W}}^{m})) +
\parallel {\bf{W}}^{d,p,e,m} \parallel ^2,
\end{split}
\label{eqn:obj_1}
\end{equation}
where $f({\bf{x}}_i; {\bf{W}})$ is a non-linear mapping function. 
${\bf{W}}^d, {\bf{W}}^p, {\bf{W}}^e, {\bf{W}}^m$ are the weights for identity, pose, expression, and illumination estimation respectively, where part of them are shared. 
$\alpha, \beta$, and $\gamma$ are used to control the importance of pose, expression, and illumination classification. 
The last term is the regularization to penalize the complexity of the weights. 
$l(\cdot)$ is the cross-entropy loss for each of the classification tasks,
\begin{equation}
l(y_i, f({\bf{x}}_i; {\bf{W}})) = -\mbox{log}(p(y_i \mid {\bf{x}}_i; {\bf{W}})),
\end{equation}
where $p(\cdot)$ is the softmax function that computes the class posterior probability.
In our network, the identity classification is considered as the main task, where the outputs from the last fully connected layer are used as the identity feature for face recognition.
Similar to prior work~\cite{xiong2015conditional}, we use the cosine distance between two identity features as the similarity metric to compare two face images. 


We use stochastic gradient descent to solve the optimization problem of Equation~\ref{eqn:obj_1} through back propagation. 
In our experiments, we have observed that sharing more layers among these tasks increases the performance, as will be presented in Table~\ref{tab:different_nets}. 
Therefore, we will build our network structure on top of $(c)$ in the remainder of this paper. 


\subsection{Task-Directed Multi-Task Learning for CNN}
\begin{figure}[t!]
\begin{center}
\includegraphics[width=0.99\textwidth]{Figures/overview_2.pdf}
\end{center}
\vspace{-5mm}
\caption{The proposed task-directed MT-CNN architecture. The top part is the same as Figure~\ref{fig:overview_1} (c). The sub-network in the bottom-right is designed to simultaneously extract pose-specific features for each of five pose groups. During the training stage, we use the ground truth pose label to assign a face image to the right pose group for feature learning. During the testing stage, the estimated pose group is used to automatically direct the path for an input image.}
\label{fig:overview_2}
\vspace{-2mm}
\end{figure}

In the aforementioned MT-CNN structure, the CNN weights of ${\bf{W}}^d$ is responsible to represent a non-linear mapping function to estimate the correct identity $y^d_i$ from a face image ${\bf{x}}_i$, with arbitrary pose, expression, and illumination.
This is clearly a very challenging learning problem for ${\bf{W}}^d$ to achieve such a powerful mapping, considering the diverse PIE variations in ${\bf{x}}_i$.
In other words, we view this learning challenge in CNN is mostly caused by the high variations encompassed in the data. 
The similar challenge has been encountered in classic pattern recognition work.
For example, in order to handle large variations in face poses, ~\cite{li2006bagging} proposes to construct several face detectors where each of them is in charge of one specific view. 
Such a divide-and-conquer scheme could potentially be applied to CNN learning as well, especially considering that in MT-CNN the side tasks already categorize the training data into multiple groups according to different tasks (or variations).
Therefore, these side tasks can ``divide'' the data and allow the CNN to better ``conquer'' them by learning better mapping functions.

Motivated by this key idea, we propose a novel task-directed MT-CNN model where the side tasks can categorize the training data into multiple groups, direct them through different routes in the network structure, and jointly learn the CNN weights for all groups, the main task, and the side tasks. 
In the context of face recognition, since pose variation is considered as the most challenging one among other variations~\cite{xiong2015conditional, zhang2013pose, zhu2014multi}, we use pose as an example to illustrate the pose-directed MT-CNN model, as shown in Figure~\ref{fig:overview_2}. 
%in face recognition, which has been studied the most in literature~\cite{xiong2015conditional, zhang2013pose, zhu2014multi}.
%Considering the fact that pose estimation is a relatively easy task, we propose a pose-directed CNN model as shown in Figure~\ref{fig:overview_2}. 

This network consists of two parts where the first part is exactly the same as in Figure~\ref{fig:overview_1} $(c)$, which is shown to be the best among all three network structures. 
The second part shares the same convolution, pooling, and the first fully connected layers as the first part. 
The novelty of the second part is to group face images with similar poses to learn pose-specific identity features. 
Specifically, we split all yaw angles in the range of $[-90^\circ, 90^\circ]$ into five different groups: right profile ($-90^\circ, -75^\circ, -60^\circ$), right half-profile ($-45^\circ, -30^\circ$), frontal ($-15^\circ, 0^\circ, 15^\circ$), left half-profile ($30^\circ, 45^\circ$), and left profile ($60^\circ, 75^\circ, 90^\circ$). 
During the training process, we use the ground truth pose labels $y^p_i$ to assign a face image into the correct group. 
Therefore, five different sets of weights, one for each group, are learned simultaneously. 
Given a face image in the testing stage, we use its estimated pose, which is the output of the pose estimation side task, to select the corresponding route to extract pose-specific identity features for that image. 
%TODO, add 1-2 sentence on HOW this is implemented in Caffe. (Added to experiments section)

Similar to Equation~\ref{eqn:obj_1}, the objective of pose-directed MT-CNN can be formulated as:
\vspace{-3mm}
\begin{equation}
\begin{split}
\underset{{\bf{W}}^{d0,dg,p,e,m}} {\mathrm{argmin}} \sum_{i=1}^{N} l(y_i^{d}, f({\bf{x}}_i; {\bf{W}}^{d0})) + \frac{1}{G}\sum_{g=1}^{G}\sum_{i=1}^{N_g} l(y_i^{d}, f({\bf{x}}_i; {\bf{W}}^{dg})) + \\
\alpha \sum_{i=1}^{N} l(y_i^{p}, f({\bf{x}}_i; {\bf{W}}^{p})) + 
\beta \sum_{i=1}^{N} l(y_i^{e}, f({\bf{x}}_i; {\bf{W}}^{e})) +
\gamma \sum_{i=1}^{N} l(y_i^{m}, f({\bf{x}}_i; {\bf{W}}^{m})), 
\end{split}
\label{eqn:obj_2}
\end{equation}
where $G = 5$ is the number of different pose groups, and $N_g$ is the number of training images in the $g$-th group. 
Similarly, the cross-entropy loss with the softmax function is used. 
The regularization terms are omitted for clarity. 

This network structure aims to learn two types of identity features. 
First, ${\bf{W}}^{d0}$ is the weights used to extract generic identity features that is robust to all poses. 
Second, $\{{\bf{W}}^{dg}\}_{g=1}^5$ are the weights used to extract pose-specific identity features that are robust within a small pose range. 
These two types of features are complementary to each other, and can both contribute to face recognition. 
Given two face images, we use the average cosine distance of these two features as the similarity metric.
%Finally, the rank-$1$ identification accuracy is reported. 

















